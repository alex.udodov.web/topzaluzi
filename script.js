// To correct navigation change all location.assing variables.
// Find keyword: location.assign

// Variables
const options = document.querySelectorAll('.option');

// Quiz Module
const quizModule = {
    blindsType: undefined,
    windowColor: undefined,
    fabricType: undefined,
    windowAmount: undefined,

    setBlindsType: function(type) {
        this.blindsType = type;
    },

    setWindowColor: function(color) {
        this.windowColor = color;
    },

    setFabricType: function(fabric) {
        this.fabricType = fabric;
    },

    setWindowAmount: function(amount) {
        this.windowAmount = amount;
    },
};

// Page language settings
const [html] = document.getElementsByTagName('html');
const lang = html.getAttribute('lang');

// Quiz variables
const quizContent = document.getElementsByName('quiz-content');
const stepIndicator = document.querySelectorAll('.step-indicator');
let b = -1;
let i = 0;
let a = 1;
let s = 2;

// Dynamically changed step number function
function stepNumber() {
    if (lang === 'ru') {
        stepIndicator[i].innerHTML = `Шаг ${s} из 5`;
    } else if (lang === 'lv') {
        stepIndicator[i].innerHTML = `Solis ${s} no 5`;
    }
}

// Next Step function
function nextStep() {
    quizContent.item(i).classList.add('hide');
    quizContent.item(a).classList.remove('hide');
    
    i++;
    a++;
    b++;
    s++;
    stepNumber();
    }

// Back Function
function back() {

    if (lang === 'ru' & i > 0) {
            quizContent.item(i).classList.add('hide');
            quizContent.item(b).classList.remove('hide');
        } else if (lang === 'ru' & i === 0) {
            // ENTER YOUR FIRST PAGE URL
            location.assign('/topzaluzi/');
        }


        if (lang === 'lv' & i > 0) {
            quizContent.item(i).classList.add('hide');
            quizContent.item(b).classList.remove('hide');
        } else if (lang === 'lv' & i <= 0) {
            // ENTER YOUR FIRST PAGE URL
            location.assign('/topzaluzi/');
        }

        i--;
        a--;
        b--;
        // Prevent appearence "Step 1" indicator while redirecting from quiz step 2 to start page
        if(s > 2) {
            s--;
        }

        stepNumber();
}


// Back Buttons
    document
    .querySelectorAll('.back-button')
    .forEach((el) => el.addEventListener('click', () => 
    {
    back();
    }
));

// Blinds Type
document
    .querySelectorAll('[data-blinds-type]')
    .forEach((el) => el.addEventListener('click', (event) =>
        {
            localStorage.setItem('blindsType', event.target.getAttribute('data-blinds-type'));
            location.assign('quiz/');
}));

// Window Color
document
    .querySelectorAll('[data-window-color]')
    .forEach((el) => el.addEventListener('click', (event) =>
        {
            quizModule.setWindowColor(event.target.getAttribute('data-window-color'));
            nextStep();
    }
));

// Fabric Type
document
    .querySelectorAll('[data-fabric-type]')
    .forEach((el) => el.addEventListener('click', (event) =>
        {
        quizModule.setFabricType(event.target.getAttribute('data-fabric-type'));
        nextStep();
    }
));

// Window Amount
document
    .querySelectorAll('[data-window-amount]')
    .forEach((el) => el.addEventListener('click', (event) =>
        {
        quizModule.setWindowAmount(event.target.getAttribute('data-window-amount'));
        nextStep();
    }
));
 
// Validation
const form = document.querySelector('#quiz-form');
const formData = new FormData(form);
const nameInput = document.getElementById('name');
const emailInput = document.getElementById('email');
const phoneInput = document.getElementById('phone');
const locationInput = document.getElementById('location');
const nameError = document.getElementById('name-error');
const emailError = document.getElementById('email-error');
const phoneError = document.getElementById('phone-error');

form.addEventListener('submit', (e) => {
    e.preventDefault();
    checkInputsValues();
    if (nameInput.value !== "" && emailInput.value !== "" && nameError.classList.contains('hide') && emailError.classList.contains('hide') && phoneError.classList.contains('hide')) {
        const request = new XMLHttpRequest();

    request.open("post", "");
    request.onload = function() {
        const userData = {
            "name": nameInput.value.trim(),
            "email": emailInput.value.trim(),
            "phone": phoneInput.value.trim(),
            "type": quizModule.blindsType,
            "color": quizModule.windowColor,
            "fabric": quizModule.fabricType,
            "amount": quizModule.windowAmount,
        };
        console.log(userData);
    }
    request.send();
    if (lang === 'ru') {
        // Redirection after Form submition RU version
        location.assign('YOUR URL')
    } else if (lang === 'lv') {
        // Redirection after Form submition LV version
        location.assign('YOUR URL')
    }
}
});


 function checkInputsValues() {
    // get the values from the inputs
    const nameValue = nameInput.value.trim();
    const emailValue = emailInput.value.trim();
    const phoneValue = phoneInput.value.trim();
        if(nameValue === '') {
            // show error
            nameError.classList.remove('hide');
            // add error class
            nameInput.parentElement.classList.add('error');
        } else {
            // add success class
            nameInput.parentElement.classList.remove('error');
            nameError.classList.add('hide');
        }
    
        if(emailValue === '' || !emailTest(emailValue)) {
            // show error
            emailError.classList.remove('hide');
            // add error class
            emailInput.parentElement.classList.add('error');
        } else {
            // add success class
            emailInput.parentElement.classList.remove('error');
            emailError.classList.add('hide');
        }
    
        if(phoneValue !== '') {
            if(!phoneTest(phoneValue)) {
                // show error
                phoneError.classList.remove('hide');
                // add error class
                phoneInput.parentElement.classList.add('error');
            } else {
                // add success class
                phoneInput.parentElement.classList.remove('error');
                phoneError.classList.add('hide');
            }
        }
    };
    
    
    //Email test function
    function emailTest(emailValue) {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(emailValue);
    };
    
    // Phone test function
    function phoneTest(phoneValue) {
        return /^\d{8}$/.test(phoneValue);
    };
    

// Quiz page onload function
function quizOnload() {
    quizModule.setBlindsType(localStorage.getItem('blindsType'));
    stepNumber();
}