<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../../style.css">
    <title>Top Zaluzi kvize</title>
</head>
<body onload='quizOnload()'>
    <section class="quiz-container">
        <!-- Step 2 -->
        <div class="step-2" name='quiz-content'>
            <!-- Main container --> 
            <div class="main-container">
                <div class="step-indicator"></div>
                <div class="back-button">&#8592</div>
                <h3>Выберите цвет рамы окна</h3>
                <div class="options-container">
                    <div class="option">
                        <img src="/images/2.1.webp" alt="" data-window-color='white'>
                        <span>Белый</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.2.webp" alt="" data-window-color='grey'>
                        <span>Анодированный алюминий (серый)</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.3.webp" alt="" data-window-color='anthracite'>
                        <span>Антроцит</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.4.webp" alt="" data-window-color='pine'>
                        <span>Сосна</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.5.webp" alt="" data-window-color='golden-oak'>
                        <span>Золотой дуб</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.6.webp" alt="" data-window-color='nut'>
                        <span>Орех</span>
                    </div>
                    <div class="option">
                        <img src="/images/2.7.webp" alt="" data-window-color='dark-brown'>
                        <span>Темно коричневый</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Step 3 -->
        <div class="step-3 hide" name='quiz-content'>
            <!-- Main container --> 
            <div class="main-container">
                <div class="step-indicator"></div>
                <div class="back-button">&#8592</div>
                <h3>Выберите тип ткани</h3>
                <div class="options-container">
                    <div id="option1" class="option">
                        <img src="/images/3.1.webp" alt="" data-fabric-type='day-night'>
                        <span>День-ночь</span>
                    </div>
                    <div id="option2" class="option">
                        <img src="/images/3.2.webp" alt="" data-fabric-type='sun-reflective'>
                        <span>Солнцеотражающие</span>
                    </div>
                    <div id="option3" class="option">
                        <img src="/images/3.3.webp" alt="" data-fabric-type='blackout'>
                        <span>Blackout</span>
                    </div>
                    <div id="option4" class="option">
                        <img src="/images/3.4.webp" alt="" data-fabric-type='budgetary'>
                        <span>Бюджетные</span>
                    </div>
                    <div id="option5" class="option">
                        <img src="/images/3.5.webp" alt="" data-fabric-type='standart'>
                        <span>Стандартные</span>
                    </div>
                    <div id="option6" class="option">
                        <img src="/images/3.6.webp" alt="" data-fabric-type='decorative'>
                        <span>Декоративные</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Step 4 -->
        <div class="step-4 hide" name='quiz-content'>
            <!-- Main container --> 
            <div class="main-container">
                <div class="step-indicator"></div>
                <div class="back-button">&#8592</div>
                <h3>Выберите количество окон</h3>
                <div class="options-container">
                    <div id="option1" class="option">
                        <img src="/images/4.1.webp" alt="" data-window-amount='1-3'>
                        <span>1-3</span>
                    </div>
                    <div id="option2" class="option">
                        <img src="/images/4.2.webp" alt="" data-window-amount='4-5'>
                        <span>4-5</span>
                    </div>
                    <div id="option3" class="option">
                        <img src="/images/4.3.webp" alt="" data-window-amount='6+'>
                        <span>6 и более</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- Step 5 -->
        <div class="step-5 hide" name='quiz-content'>
            <div class="main-container">
                <div class="step-indicator"></div>
                <div class="back-button">&#8592</div>
                <div class="congrats">Вы успешно ответили на все вопросы!</div>
                <h3>Куда Вам выслать предложение по выбранным жалюзи?</h3>
                <!--Form-->
                <form method="POST" id='quiz-form'>
                    <!-- Name -->
                    <div class="form-subcontainer">
                        <img class="icon" src="../../images/name.svg" alt="">
                        <input class="_req" id="name" type="text" placeholder="Ваше имя *" name='name' >
                    </div>
                    <!-- Name error message-->
                    <div class="error-message hide" id="name-error">Вы не указали имя</div>
                    <!-- E-mail -->
                    <div class="form-subcontainer">
                        <img class="icon" src="../../images/email.svg" alt="">
                        <input class="_req" id="email" type="email" placeholder="Электронная почта *" name='email'>
                    </div>
                    <!-- Email error message -->
                    <div class="error-message hide" id="email-error">Неверная электронная почта</div>
                    <!-- Phone -->
                    <div class="form-subcontainer">
                        <img class="icon jsInputPhone" src="../../../images/phone.svg" alt="">
                        <input type="tel" id="phone" placeholder="Номер телефона" name='phone'>
                    </div>
                    <!-- Phone error message -->
                    <div class="error-message hide" id="phone-error">Неверный номер телефона</div>
                    <!-- Location -->
                    <div class="form-subcontainer">
                        <input type="text" id="location" placeholder="Город (населённый пункт)" name='location'>
                    </div>
                    <!-- Submit button -->
                    <button type="submit" name="submit" id="form-button">Получить предложение</button>
                </form>
                <!-- Personal data rules -->
                <div class="data-rules-container">Нажимая на кнопку "получить предложения" Вы соглашаетесь с <a href="#popupWrapper" class="step-link">правилами обработки персональных данных</a></div>
            </div>
        </div>
    </section>
    <script src="../../script.js"></script>
</body>
</html>