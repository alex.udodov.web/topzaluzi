<?php

$request = $_SERVER['REQUEST_URI'];
$extraHead = '';

switch ($request) {
    // Main page
    case '/topzaluzi/' :
        require __DIR__ . '/pages/main.php';
        break;

    // Quiz page
    case '/topzaluzi/quiz/' :
        require __DIR__ . '/pages/quiz.php';
        break;
    
    // Error 404
    default:
        http_response_code(404);
        require __DIR__ . '/pages/404.php';
        break;
}
